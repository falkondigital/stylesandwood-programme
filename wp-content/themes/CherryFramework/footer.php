<?php /*
		<footer class="motopress-wrapper footer">
			<div class="container">
				<div class="row">
					<div class="<?php echo cherry_get_layout_class( 'full_width_content' ); ?>" data-motopress-wrapper-file="wrapper/wrapper-footer.php" data-motopress-wrapper-type="footer" data-motopress-id="<?php echo uniqid() ?>">
						<?php get_template_part('wrapper/wrapper-footer'); ?>
					</div>
				</div>
			</div>
		</footer>
		<!--End #motopress-main-->
	*/ ?>
<footer id="footertop">

	<div class="container">

		<div class="row">



			<div class="col-md-4">


                <h3>Useful Links</h3>

                <p>
                    <a href="http://www.stylesandwood-group.co.uk/" target="_blank" rel="nofollow">Styles&amp;Wood Group plc</a><br>
                    <a href="http://www.stylesandwood.co.uk/" target="_blank" rel="nofollow">Styles&amp;Wood</a><br>
                    <a href="http://www.keysource.co.uk/ " target="_blank" rel="nofollow">Keysource</a><br>
                    <a href="http://www.gdmpartnership.co.uk/ " target="_blank" rel="nofollow">GDM Partnership</a><br>
                </p>

<!--				<h3>Useful Links</h3>-->
<!---->
<!--				<p><a href="/disclaimer/">Disclaimer</a><br />-->
<!---->
<!--					<a href="/sitemap/">Sitemap</a><br />-->
<!---->
<!--					<a href="/contactus/">Contact Us</a></p>-->

			</div>



			<?php dynamic_sidebar('footer-sidebar'); ?>



			<div class="col-md-4">

                <br><br><br>
                <a href="<?php echo home_url(); ?>/contact-us/">Contact Us</a><br>
                <a href="http://www.stylesandwood-group.co.uk/disclaimer/">Disclaimer</a><br>
                <a href="http://www.stylesandwood.co.uk/joinus/" target="_blank">Vacancies</a><br>
                <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Modern-Slavery-Statement.pdf" target="_blank">Modern Slavery Statement </a><br>
                <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Health-Safety.pdf" target="_blank">Health & Safety Policy </a><br>
                <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Quality-Policy.pdf" target="_blank">Quality Policy </a><br>
                <a href="http://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Environmental-Policy.pdf" target="_blank">Environmental Policy</a>

			</div>





			<div class="col-md-4">

<!--                <h3>Share Price</h3>-->
<!---->
<!--                <iframe class="share-iframe" frameborder=0 src="http://ir.tools.investis.com/Clients/uk/styles_and_wood1/Ticker/Ticker.aspx?culture=en-GB"></iframe><br>-->

                <!-- Begin MailChimp Signup Form -->

<!--				<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">-->
<!---->
<!--				<style type="text/css">-->
<!---->
<!--					#mc_embed_signup{clear:left; font:14px Helvetica,Arial,sans-serif; }-->
<!---->
<!--					#mce-success-response{color:#fff;}-->
<!---->
<!--					#mc_embed_signup div.response{padding:0px;}-->
<!---->
<!--				</style>-->
<!---->
<!--				<div id="mc_embed_signup">-->
<!---->
<!--					<form action="//stylesandwood-group.us8.list-manage.com/subscribe/post?u=54bef7046832cb4cfa0334e98&amp;id=25bf41502e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>-->
<!---->
<!--						<div id="mc_embed_signup_scroll">-->
<!---->
<!--							<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>-->
<!---->
<!--							<div class="mc-field-group">-->
<!---->
<!--								<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>-->
<!---->
<!--								</label>-->
<!---->
<!--								<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">-->
<!---->
<!--							</div>-->
<!---->
<!--							<div class="mc-field-group">-->
<!---->
<!--								<label for="mce-FNAME">First Name </label>-->
<!---->
<!--								<input type="text" value="" name="FNAME" class="" id="mce-FNAME">-->
<!---->
<!--							</div>-->
<!---->
<!--							<div class="mc-field-group">-->
<!---->
<!--								<label for="mce-LNAME">Last Name </label>-->
<!---->
<!--								<input type="text" value="" name="LNAME" class="" id="mce-LNAME">-->
<!---->
<!--							</div>-->
<!---->
<!--							<div id="mce-responses" class="clear">-->
<!---->
<!--								<div class="response" id="mce-error-response" style="display:none"></div>-->
<!---->
<!--								<div class="response" id="mce-success-response" style="display:none"></div>-->
<!---->
<!--							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<!---->
<!--							<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_54bef7046832cb4cfa0334e98_25bf41502e" tabindex="-1" value=""></div>-->
<!---->
<!--							<div class="clear"><input type="submit" value="Subscribe to our mailing list" name="subscribe" id="mc-embedded-subscribe" class="button"></div>-->
<!---->
<!--						</div>-->
<!---->
<!--					</form>-->
<!---->
<!--				</div>-->
<!---->
<!--				<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>-->

				<!--End mc_embed_signup-->

			</div>





		</div>

	</div>





	<div class="container">



		<div class="row">

			<div class="span12">

				<div class=" fixclear">

					<ul class="social-icons colored fixclear"><li class="title">See more from the Styles&amp;Wood Group</li><li class="social-twitter"><a target="_blank" href="https://twitter.com/StylesandWood">Twitter</a></li><li class="social-linkedin"><a target="_blank" href="http://www.linkedin.com/company/78033">LinkedIn</a></li></ul>

					<div class="copyright">

						<a href="http://www.stylesandwood-group.co.uk"><img alt="Styles&amp;Wood Group PLC" src="http://stylesandwood.co.uk/sw-cms/wp-content/uploads/SylesWood-sm.png"></a><p>&copy; 2017 Copyright - Styles&amp;Wood Group PLC</p>

					</div><!-- end copyright -->







				</div><!-- end bottom -->

			</div>

		</div>



	</div>

</footer>
</div>
<div id="back-top-wrapper" class="visible-desktop">
	<p id="back-top">
		<?php echo apply_filters( 'cherry_back_top_html', '<a href="#top"><span></span></a>' ); ?>
	</p>
</div>
<div class="nav-overlay-cover"></div>
<?php if(of_get_option('ga_code')) { ?>
	<script type="text/javascript">
		<?php echo stripslashes(of_get_option('ga_code')); ?>
	</script>
	<!-- Show Google Analytics -->
<?php } ?>

<?php wp_footer(); ?> <!-- this is used by many Wordpress features and for plugins to work properly -->
<script>
	/*** Navigation in responsive layouts
	 --------------------------------------------------- ****/
	$('.main-nav').clone(true).appendTo('body').addClass('nav-tablet');

	if ( ! $('.nav-overlay-cover').legnth){
		$('<div class="nav-overlay-cover"></div>').appendTo('.pageWrapper');
	}

	$('.nav-tablet .menu li').has('ul').addClass('has-ul');
	$('.nav-tablet li.has-ul > a').on('click', this, function(e){
		$(this).next('ul').toggle();
		e.preventDefault();
	});

	$('.nav-button').on('click', this, function(){

		$(this).toggleClass('open');
		$('.nav-tablet, .nav-overlay-cover').toggleClass('open');

	});

	$('.nav-overlay-cover').on('click', function() {
		$(this).toggleClass('open');
		$('.nav-tablet, .nav-button').toggleClass('open');
	});


</script>
</body>
</html>