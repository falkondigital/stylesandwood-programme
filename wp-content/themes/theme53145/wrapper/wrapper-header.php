<?php /* Wrapper Name: Header */ ?>
<div class="hashAncor" id="homePage"></div>
<div class="wide">
	<div class="" data-motopress-type="static" data-motopress-static-file="static/static-logo.php">
		<?php get_template_part("static/static-logo"); ?>
	</div>
	<div class=" hidden-phone" data-motopress-type="static" data-motopress-static-file="static/static-search.php">
		<?php get_template_part("static/static-search"); ?>
	</div>
	<div class="menu-holder menu-hidden">
		<div class="menu-icon"></div>
		<div class="menu-close-icon"></div>
		<div class="menu-overlay"></div>
		<div class="menu-wrapper" data-motopress-type="static" data-motopress-static-file="static/static-nav.php">
		<?php get_template_part("static/static-nav"); ?>
		</div>
	</div>
</div>