jQuery(function() 
{
    var _window = jQuery(window), 
        MSIE9 = false;
        
    if(jQuery.browser.msie){
         if(parseInt(jQuery.browser.version) <= 9){
            MSIE9 = true;
         }
    }
    
    // setTimeout(function(){
    //     jQuery('section.parallax_section').css({visibility:"visible", opacity:0}).stop().animate({opacity:1}, 1500);   
    // },50);
    
    jQuery('.main-holder').append( jQuery('.menu-holder') );
    

    
    
    var menuHolder = jQuery('.menu-holder'),
        menuBtn = jQuery('.menu-icon', menuHolder),
        menuCloseBtn = jQuery('.menu-close-icon', menuHolder), 
        menuOverlay = jQuery('.menu-overlay', menuHolder),
        menuWrapper = jQuery('.menu-wrapper', menuHolder);
        menuItems = jQuery('#topnav > .menu-item', menuWrapper);
    
    menuBtn.click(openMenu);
    menuCloseBtn.click(closeMenu);
    
    function openMenu(){
        menuHolder.removeClass('menu-hidden');
        menuOverlay.stop().animate({width:'237px'}, 1000, 'easeOutQuart');
        menuBtn.stop().animate({opacity:'0'}, 1000, 'easeOutQuart');
        menuCloseBtn.stop().delay(0).animate({top:'32px'}, 1500, 'easeOutQuart');
        
        menuItems.each(function(){
            _this = jQuery(this);
            _this.stop().delay((_this.index() * 85)+300).animate({left:'0px'}, 800, 'easeOutQuart');
        })
    }
    
    function closeMenu(){
        menuCloseBtn.stop().animate({top:'-75px'}, 1500, 'easeOutQuart');
        menuBtn.stop().delay(350).animate({opacity:'1'}, 1000, 'easeOutQuart');
        menuOverlay.stop().animate({width:'0px'}, 1000, 'easeOutQuart', function(){
            menuHolder.addClass('menu-hidden');
        });
        
        menuItems.each(function(){
            _this = jQuery(this);
            _this.stop().animate({left:'290px'}, 1000, 'easeOutQuart');
        })
    }    


     var FWObjects = $('.wide'),
        contentHolder = $('.content-holder > .container'),
        not_resizes = false;
  
    if(FWObjects.length > 0) {
        FWObjectsResize();
        _window.resize(FWObjectsResize);
    }
    
    function FWObjectsResize(){
        if(!not_resizes){
            var _windowWidth = _window.width(),
                contentHolderWidth = contentHolder.width();

                if (jQuery('body').hasClass('cherry-fixed-layout')) {

                    _windowWidth = jQuery('.main-holder').width();
                }
             
            FWObjects.css({width:_windowWidth+5, left:-(_windowWidth-contentHolderWidth)/2});
        }
    }
    

    
    var portfolio = $('.portfolio-shortcode .portfolio_wrapper'),
        portfolio_item_selector = '.portfolio-item',
        portfolio_item = $(portfolio_item_selector, portfolio),
        portfolio_columns_init = portfolio.data('columns'),
        transitionDuration = '0.5',
        filterButtons = $('.portfolio-shortcode .portfolio_filter_buttons > .filter_button'),
        currentCategory = '*';
    
    if(portfolio.length > 0) {
        portfolio.imagesLoaded( function() {
            setTimeout(function(){
                setColumnsNumber();
                resizePortfolioItem();
                portfolio.isotope({
                    itemSelector: portfolio_item_selector,
                    resizable : true,
                    layoutMode: 'masonry'
                }).bind("resize.rainbows", function(){
                    setColumnsNumber();
                    resizePortfolioItem();
                    portfolio.isotope('reLayout');
                });
            },10);
        });
        
        filterButtons.on( 'click', function() {
            var _this = $(this);
            var category = _this.attr('data-filter');
            
            if(currentCategory != category){
                filterButtons.removeClass('current-category');
                _this.addClass('current-category');
                currentCategory = category;
                if(category != '*') category = '.'+category;
                portfolio.isotope({ filter: category});
            }
        });
        
        
        $('.portfolio_wrapper .portfolio-item').magnificPopup({
            delegate: '.thumbnail > a',
            type: 'image',
            removalDelay: 500,
            mainClass: 'mfp-zoom-in',
            callbacks: {
                beforeOpen: function() {
                    // just a hack that adds mfp-anim class to markup 
                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                },
                open: function() {
                  not_resizes = true;
                },
                close: function() {
                  not_resizes = false;
                }
            },
            gallery: {enabled:true}
        });
    }
    
    function setColumnsNumber() {
        if(_window.width() < 450){
            portfolio_columns = Math.ceil(portfolio_columns_init/3);  
        } else if (_window.width() < 767){
            portfolio_columns = Math.ceil(portfolio_columns_init/2);  
        } else {
            portfolio_columns = portfolio_columns_init;  
        }
    }
    
    function resizePortfolioItem(){
        item_width = parseInt(portfolio.width() / portfolio_columns);
        portfolio_item.each(function(){
            _this = $(this);
            if(_this.hasClass('portfolio-item-highlight') && portfolio_columns > 2 ){
                _this.width(item_width*2); 
            } else {
                _this.width(item_width);   
            }
        })
    }
    
    
});

jQuery.fn.addCustomClass = function(number, customClass)
{ 
    var items = jQuery(this);
    
    for( i=0; i<items.length; i++ ){
        _this = items.eq(i);
        _this.addClass(customClass+((i % number)+1));
    }
}

jQuery('a.btn').append(jQuery('<strong></strong><strong></strong><strong></strong><strong></strong>'));

jQuery(document).ready(function(){

    var
        _window = jQuery(window)
    ;

    _window.on("resize", function(){
        resizeFunction();
    })
    resizeFunction();

    function resizeFunction(){
        var
            newWidth = _window.width()
        ,   marginHalf = _window.width()/-2;
        ;
        if (jQuery('body').hasClass('cherry-fixed-layout')) {
            var
                newWidth = jQuery('.main-holder').width()
            ,   marginHalf =jQuery('.main-holder').width()/-2;
            ;   
        }

        jQuery('.wide').css({width: newWidth, "margin-left": marginHalf, left: '50%'});
    }

    jQuery('.sf-menu>li>a').each(function(){
        $(this).attr("data-hover", $(this).text());
    });

    jQuery('body').find('.main-holder').each(function(){
       _this = jQuery(this);
       _this.find('.title-header').insertAfter(_this.find('.breadcrumb__t'));
      });
});




