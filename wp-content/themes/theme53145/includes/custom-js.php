<?php
	add_action( 'wp_enqueue_scripts', 'cherry_child_custom_scripts' );
		function cherry_child_custom_scripts() {
		wp_enqueue_script( 'theme_script', CHILD_URL . '/js/theme_script.js', array( 'jquery' ), '1.0', true );	
		wp_enqueue_script( 'cv_script', CHILD_URL . '/js/cv_script.js', array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'mousewheel', get_stylesheet_directory_uri() . '/js/jquery.mousewheel-3.0.4.js', array( 'jquery' ), '1.0', true );

	} 
?>